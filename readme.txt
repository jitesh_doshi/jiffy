Jiffy - CMS

Organization:
  main.go - main func (simply calls jiffy.Main)
  modules.go - imports all modules to be compiled into the binary
  jiffy/jiffy.go - jiffy core framework
  jiffy/api.go - jiffy public API
  core/ - core (mostly required) modules
  core/node.go - node manipulation module
  templates/node/edit.html - node create/edit template
  templates/node/list.html - node listing template
  templates/node/view.html - single node display template

Jiffy is a CMS application and an extensible platform. It is written in
Go programming language (http://golang.org/). It provides basic CMS
functionality, and, more importantly, allows you to extend it. It is
designed with extensibility in mind.

Features:
- Super fast (most pages are loaded in under 25ms, including DB calls)
- 

// Sample Jiffy module - demonstrates the Jiffy API and how to develop modules and extend Jiffy.
// Please see api.go to see all the methods your module can implement to extend Jiffy.
package samples

// import the packages your module needs
import (
	"log"
	// you must import "jiffy/jiffy" to use Jiffy API.
	. "jiffy/jiffy" // "." at the beginning imports Jiffy API into current namespace
	"net/http"
)

// Every module needs an init() func so that it can register itself at startup
func init() {
	Register(new(module), "hello-jiffy")
}

// You need a type to implement this module's interface methods
type module struct {
	// this module has no global state, so this type contains no members
}

func (m *module) InitRequest(*http.Request) {
	// this is where you can do something at the beginning of each request
	log.Println("hello-jiffy module: InitRequest")
}

func (m *module) Handlers() []*Handler {
	// this is where you can specify which URI path your module can handle
	log.Println("hello-jiffy module: Handlers")
	return []*Handler{
		&Handler{
			Path:     "/hello-jiffy/", // the path being handled
			Callback: helloHandler,    // func that handles this path
			CallbackArgs: []interface{}{ // args given to your callback
				R(":"),          // entire request object
				R(":method"),    // request method
				R("p1"),         // request parameter
				R("p2"),         // request parameter
				H("User-Agent"), // header
				P(0),            // URI path component 0
				P(1),            // URI path component 1				
			},
			OutputMapper: T{GetTemplate("hello-jiffy"), "/hello-jiffy/foo"},
		},
	}
}

type hello struct {
	req                                      *http.Request
	method, param1, param2, ua, path0, path1 string
}

func helloHandler(req *http.Request, method, param1, param2, ua, path0, path1 string) interface{} {
	return hello{
		req,
		method,
		param1,
		param2,
		ua,
		path0,
		path1,
	}
}

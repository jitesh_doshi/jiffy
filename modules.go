package main

// This file exists simply so that you can import modules into it.
// This file must be a part of jiffy's "main" package.
//
// Add your modules below. place "_" in front of them so that you don't
// get compiler error for unused package.
import (
	_ "jiffy/core"
	_ "jiffy/samples"
	// _ "modules/m2"
)

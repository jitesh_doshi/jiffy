// Jiffy CMS - Superfast Content Management System.
// 
// Do not import this package into your module code. Import "jiffy/jiffy" instead.
// That is the one that contains all public APIs for Jiffy module development.
// At this point, to extend Jiffy, you must compile your modules into the jiffy
// binary.
// To add your own packages to jiffy binary, just edit the modules.go file
// in this package, import your modules and recompile jiffy.
//
// See http://jiffy.doshiland.com/ for detailed information on Jiffy and extending it.
package main

import "jiffy/jiffy"

func main() {
	// just call jiffy's Main
	jiffy.Main()
}

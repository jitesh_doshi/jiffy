// Jiffy public API for Jiffy modules
package jiffy

import (
	"errors"
	"fmt"
	"text/template"
	"io/ioutil"
	"log"
	"net/http"
)

// Implement if you want your module to capture request init
type InitRequest interface {
	// request init hook - run at request init
	InitRequest(*http.Request)
}

// Implement if you want your module to handle HTTP requests (read data
// from requests and produce data for responses).
type Handlers interface {
	// URI path handler registration hook - run at app init. Module should
	// return a slice of Handler structs containing the paths they handle,
	// the callbacks they use and the input and output mappers etc.
	Handlers() []*Handler
}

// map of registered modules
var modules = make(map[string]interface{})

// Registers a module with Jiffy. The first param is any piece of data (any
// type) and second is the string name of the module.
//
// Trying to register a module name that is already taken is okay. It just
// unregisters the other one and registers the new one.
func Register(m interface{}, name string) error {
	log.Println("Registering module: ", name)
	if _, ok := modules[name]; ok {
		msg := fmt.Sprintf("Module already registered: %s", name)
		log.Println(msg)
		return errors.New(msg)
	}
	modules[name] = m
	if m, ok := m.(Handlers); ok {
		// get module's URI path handlers and register with the HTTP server
		for _, h := range m.Handlers() {
			if h.Path != "" {
				http.Handle(h.Path, http.Handler(h)) // register
			}
		}
	}
	return nil
}

var tmpl = template.New("tmpl")

// template.Must(template.ParseFiles(getTemplatePath("html.html")))

func GetTemplate(name string) *template.Template {
	t := tmpl.Lookup(name)
	if t == nil {
		path := getTemplatePath(name)
		t = getNewTemplate(name, path, tmpl)
	}
	return t
}

func getNewTemplate(name, path string, parent *template.Template) *template.Template {
	log.Println("Parsing template file:", path)
	s, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var t *template.Template
	if parent == nil {
		t = template.New(name)
	} else {
		t = parent.New(name)
	}
	return template.Must(t.Parse(string(s)))
}

func getTemplatePath(name string) string {
	return "templates/" + name + ".html"
}

package jiffy

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"text/template"
)

// An InputMapper reads the request and returns a value from the Request. It
// may also manipulate the ResponseWriter in some way if necessary.
type InputMapper interface {
	mapInput(w http.ResponseWriter, req *http.Request) interface{}
}

// Input Mapper to get params out of a URI path
type P int

// Input Mapper to get params (GET or POST) out of a request
type R string

// Input Mapper to get header values out of a request
type H string

// Input Mapper to get Persistence Context (Database)
type D string

func (m P) mapInput(w http.ResponseWriter, req *http.Request) interface{} {
	index := int(m)
	parts := strings.Split(req.URL.Path[1:], "/")
	return parts[index]
}

func (m R) mapInput(w http.ResponseWriter, req *http.Request) interface{} {
	name := string(m)
	// special components start with ":"
	if name[0:1] == ":" {
		switch name[1:] {
		case "method":
			// ":method" maps to the request method
			return req.Method
		case "":
			// ":" maps to the request itself
			return req
		}
	}
	return req.FormValue(name)
}

func (m H) mapInput(w http.ResponseWriter, req *http.Request) interface{} {
	name := string(m)
	return req.Header.Get(name)
}

func (m D) mapInput(w http.ResponseWriter, req *http.Request) interface{} {
	return PersistenceRequestVar.Get(req)
}

// An OutputMapper reads the result from a handler function and writes it
// to the ResponseWriter. It may also consult the Request if necessary.
type OutputMapper interface {
	mapOutput(w io.Writer, req *http.Request, result interface{})
}

// Output Mapper for templates. It looks up the template and executes it
// using the result as data
type T struct {
	Template     *template.Template
	PostRedirect string
}

func (t T) mapOutput(w io.Writer, req *http.Request, result interface{}) {
	if w, ok := w.(http.ResponseWriter); ok {
		if t.PostRedirect != "" && req.Method == "POST" {
			http.Redirect(w, req, t.PostRedirect, http.StatusFound)
		} else {
			w.Header().Set("Content-Type", "text/html")
		}
	}
	err := t.Template.Execute(w, result)
	if err != nil {
		panic(err)
	}
}

type HTML struct {
	tag        string
	attributes map[string]string
}

func (h HTML) mapOutput(w io.Writer, req *http.Request, result interface{}) {
	// write opening tag with attributes
	w.Write([]byte("<"))
	template.HTMLEscape(w, []byte(h.tag))
	for key, val := range h.attributes {
		template.HTMLEscape(w, []byte(key))
		w.Write([]byte("=\""))
		template.HTMLEscape(w, []byte(val))
		w.Write([]byte("\""))
	}
	w.Write([]byte(">"))
	// write result
	DefaultOutputMapper{}.mapOutput(w, req, result)
	// write closing tag
	w.Write([]byte("</"))
	template.HTMLEscape(w, []byte(h.tag))
	w.Write([]byte(">"))
}

// Default Output Mapper for string, Stringer, Reader results. It simply writes
// the result to the response writer
type DefaultOutputMapper struct{}

func (m DefaultOutputMapper) mapOutput(w io.Writer, req *http.Request, result interface{}) {
	switch r := result.(type) {
	case string:
		w.Write([]byte(r))
	case fmt.Stringer:
		w.Write([]byte(r.String()))
	case io.Reader:
		bytes, err := ioutil.ReadAll(r)
		if err != nil {
			panic(err)
		}
		w.Write(bytes)
	default:
		fmt.Fprintf(w, "%#v\n", result)
	}
}

// type Callback func(args ...interface{}) interface{}
type Callback interface{}

type Handler struct {
	Title        string
	Callback     Callback
	Path         string
	CallbackArgs []interface{}
	OutputMapper OutputMapper
}

type TemplateContext struct {
	Content string
	Title   string
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	h.Init()
	num_args := len(h.CallbackArgs)
	args := make([]reflect.Value, num_args)
	for i, arg := range h.CallbackArgs {
		args[i] = reflect.ValueOf(resolveArg(arg, w, req))
	}
	// result := h.Callback(args...)
	results := reflect.ValueOf(h.Callback).Call(args)
	var b bytes.Buffer
	for _, r := range results {
		h.OutputMapper.mapOutput(&b, req, r.Interface())
	}
	tc := &TemplateContext{b.String(), h.Title}
	GetTemplate("html").Execute(w, tc)
}

func (h *Handler) Init() *Handler {
	if h.Callback == nil {
		panic("Callback is required")
	}
	if h.Path == "" {
		panic("Path is required")
	}
	if h.OutputMapper == nil {
		h.OutputMapper = DefaultOutputMapper{}
	}
	return h
}

func resolveArg(arg interface{}, w http.ResponseWriter, req *http.Request) interface{} {
	if m, ok := arg.(InputMapper); ok {
		mapped := m.mapInput(w, req)
		return resolveArg(mapped, w, req)
	}
	return arg
}

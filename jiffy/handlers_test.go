package jiffy_test

import (
	. "jiffy/jiffy"
	"net/http"
	"net/http/httptest"
	"testing"
)

func myfunc(args ...interface{}) interface{} {
	return "something"
}

func TestHandlers(t *testing.T) {
	handler := (&Handler{Callback: Callback(myfunc)}).Init()
	if handler.Path != "/" {
		t.Logf(`Failed: Path != "/"`)
	} else {
		t.Logf(`Good: Path == "/"`)
	}
	httptest.NewServer(http.Handler(handler))
	// t.Fail()
}

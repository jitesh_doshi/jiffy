package jiffy

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

var config = flag.String("config", "jiffy.conf", "path to the JSON format config file")

type ConfigMap map[string]interface{}

func mapget(m map[string]interface{}, defaultValue interface{}, path ...string) interface{} {
	if len(path) == 1 {
		if val, ok := m[path[0]]; ok {
			return val
		}
	} else if len(path) > 1 {
		if val, ok := m[path[0]]; ok {
			return mapget(val.(map[string]interface{}), defaultValue, path[1:]...)
		}
	}
	return defaultValue
}

func (c ConfigMap) Get(defaultValue interface{}, path ...string) interface{} {
	return mapget((map[string]interface{})(c), defaultValue, path...)
}

func (c ConfigMap) GetString(defaultValue string, path ...string) string {
	return c.Get(defaultValue, path...).(string)
}

var Config ConfigMap

func initConfig() {
	// flags are not available until you call flag.Parse
	flag.Parse()
	f, err := os.Open(*config)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(&Config)
	if err != nil {
		panic(fmt.Errorf("JSON config(%s): %s", *config, err))
	}
}

func init() {
	initConfig()
}

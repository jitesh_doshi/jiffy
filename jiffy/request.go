package jiffy

import (
	"net/http"
	"sync"
)

// thanks to https://groups.google.com/group/golang-nuts/msg/e2d679d303aa5d53

type RequestVar struct {
	l sync.Mutex
	m map[*http.Request]interface{}
}

func (v *RequestVar) Set(req *http.Request, val interface{}) {
	v.l.Lock()
	defer v.l.Unlock()
	if v.m == nil {
		v.m = make(map[*http.Request]interface{})
	}
	v.m[req] = val
}

func (v *RequestVar) Get(req *http.Request) interface{} {
	v.l.Lock()
	defer v.l.Unlock()
	if v.m == nil {
		return nil
	}
	return v.m[req]
}

func (v *RequestVar) Clear(req *http.Request) {
	v.l.Lock()
	defer v.l.Unlock()
	delete(v.m, req)
}

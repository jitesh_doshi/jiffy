// mgo-based persistence

package jiffy

import (
	"labix.org/v2/mgo"
	"log"
)

type driver struct {
}

func (d *driver) Dial(connectionString string) (p Persistence, err error) {
	log.Println("Connecting to:", connectionString)
	session, err := mgo.Dial(connectionString)
	if err != nil {
		panic(err)
	}

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	p = &pc{session, "jiffy"}
	return
}

type pc struct {
	session *mgo.Session
	db      string
}

func (p *pc) Save(typ string, data interface{}, query interface{}) (err error) {
	if query != nil {
		_, err = p.session.DB(p.db).C(typ).Upsert(query, data)
	} else {
		err = p.session.DB(p.db).C(typ).Insert(data)
	}
	return
}

func (p *pc) LoadOne(typ string, data interface{}, query interface{}) (err error) {
	err = p.session.DB(p.db).C(typ).Find(query).One(data)
	return
}

func (p *pc) LoadLimited(typ string, data interface{}, query interface{}, start int, count int) (err error) {
	err = p.session.DB(p.db).C(typ).Find(query).Limit(count).Iter().All(data)
	return
}

func (p *pc) Purge(typ string, query interface{}) (err error) {
	err = p.session.DB(p.db).C(typ).Remove(query)
	return
}

func (p *pc) Close() error {
	log.Println("Disconnecting ...")
	p.session.Close()
	return nil
}

func init() {
	RegisterDriver("mgo", new(driver))
}

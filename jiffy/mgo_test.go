package jiffy_test

import (
	"jiffy/core"
	"jiffy/jiffy"
	"testing"
)

func TestDriver(t *testing.T) {
	p, err := jiffy.GetPersistence("mgo", "localhost")
	if err != nil {
		t.Fatal("GetPersistence", err)
	}

	n := core.NewNode("test title", "test body")
	core.NodeSave(n)
	var nodes []*core.Node
	p.Load("nodes", &nodes, nil)
	if len(nodes) == 0 {
		t.Logf("No nodes returned")
	}
	if len(nodes) > 0 {
		for _, n := range nodes {
			t.Logf("Node:%#v", n)
		}

	}
	t.Fail()
}

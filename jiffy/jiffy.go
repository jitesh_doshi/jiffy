// main package of jiffy
package jiffy

import (
	"log"
	"net"
	"net/http"
	"net/http/fcgi"
)

var PersistenceRequestVar *RequestVar = new(RequestVar)

// main HTTP/FCGI handler for the server
func handler(w http.ResponseWriter, req *http.Request) {
	pers, err := GetPersistence("", "")
	if err != nil {
		// TODO HTTP 500
		panic(err)
	}
	PersistenceRequestVar.Set(req, pers)
	// don't allow panics from called funcs to crash the server
	defer func() {
		// recover, log the error and return HTTP error status code
		if err := recover(); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		PersistenceRequestVar.Get(req).(Persistence).Close()
		PersistenceRequestVar.Clear(req)
	}() // define and call anonymous func
	initRequest(req)
	// all modules register themselves as handlers for specific paths
	// so just run the main http server
	http.DefaultServeMux.ServeHTTP(w, req)
}

// execute init hooks for the current request
func initRequest(req *http.Request) {
	for _, module := range modules {
		if m, ok := module.(InitRequest); ok {
			m.InitRequest(req)
		}
	}
}

// Jiffy's main func. Parses command-line options and runs the server.
func Main() {
	// attach a static file server if docroot was configured
	docroot := Config.GetString("", "docroot")
	if docroot != "" {
		http.Handle("/", http.FileServer(http.Dir(docroot)))
	}
	// start HTTP listener if http was configured
	http_listen := Config.GetString("", "http")
	if http_listen != "" {
		go func() {
			err := http.ListenAndServe(http_listen, http.HandlerFunc(handler))
			if err != nil {
				panic(err)
			}
		}()
	}
	// start FCGI listener if -http option was given on startup
	fcgi_listen := Config.GetString("", "fcgi")
	if fcgi_listen != "" {
		go func() {
			listener, err := net.Listen("tcp", fcgi_listen)
			if err != nil {
				panic(err)
			}
			fcgi.Serve(listener, http.HandlerFunc(handler))
		}()
	}
	// create a temp chan and block on it so that we don't fall off the end
	// of main and take all go routines down with us.
	<-make(chan int)
}

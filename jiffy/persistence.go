// database connectivity and persistence

package jiffy

import (
	"fmt"
	"log"
)

type Persistence interface {
	Save(typ string, data interface{}, query interface{}) error
	LoadOne(typ string, data interface{}, query interface{}) error
	LoadLimited(typ string, data interface{}, query interface{}, start int, count int) error
	Purge(typ string, query interface{}) error
	Close() error
}

type PersistenceDriver interface {
	Dial(connectionString string) (Persistence, error)
}

var drivers = make(map[string]PersistenceDriver)

func RegisterDriver(name string, driver PersistenceDriver) {
	log.Println("Registering driver:", name)
	drivers[name] = driver
}

func GetPersistence(driverName string, connectionString string) (p Persistence, err error) {
	if driverName == "" {
		driverName = Config.GetString("mgo", "persistence", "driver")
	}
	if driver, ok := drivers[driverName]; ok {
	if connectionString == "" {
		connectionString = Config.GetString("localhost", "persistence", "connectionstring")
	}
		p, err = driver.Dial(connectionString)
	} else {
		err = fmt.Errorf("template: no files named in call to ParseFiles")
	}
	return
}

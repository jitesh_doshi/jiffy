// The core package provides core modules that are bundled with Jiffy and
// most of them are needed for it to function at all.
package core

import (
	"errors"
	. "jiffy/jiffy"
	"labix.org/v2/mgo/bson"
	"log"
)

// Node is the basic unit of storage in Jiffy. It can represent a page
// stored in the DB.
type Node struct {
	// title needs to be a short string without HTML tags
	Title string
	// body tends to be longer and can contain HTML tags
	Body string
	// binary representation of the node's primary key in DB
	Id bson.ObjectId "_id,omitempty"
}

func NewNode(title, body string) *Node {
	n := new(Node)
	n.Title = title
	n.Body = body
	return n
}

func (n *Node) GenerateId() {
	if n.Id != "" {
		n.Id = bson.NewObjectId()
	}
}

func (n *Node) SetId(s string) {
	n.Id = bson.ObjectIdHex(s)
}

func (n *Node) HasId() bool {
	return len(n.Id) != 0
}

func (n *Node) Selector() bson.M {
	if n.HasId() {
		return bson.M{"_id": n.Id}
	}
	return nil
}

func IdSelector(s string) bson.M {
	return bson.M{"_id": bson.ObjectIdHex(s)}
}

type module struct {
}

func init() {
	m := &module{}
	Register(m, "node")
}

func (m *module) Handlers() []*Handler {
	return []*Handler{
		&Handler{
			Path:         "/node",
			Callback:     getNodes,
			CallbackArgs: []interface{}{D("")},
			OutputMapper: T{GetTemplate("node_list"), ""},
		},
		&Handler{
			Path:         "/node/",
			Callback:     getNode,
			CallbackArgs: []interface{}{D(""), P(1)},
			OutputMapper: T{GetTemplate("node_view"), ""},
		},
		&Handler{
			Path:         "/node/edit/",
			Callback:     editNode,
			CallbackArgs: []interface{}{D(""), P(2), R(":method"), R("title"), R("body")},
			OutputMapper: T{GetTemplate("node_edit"), "/node"},
		},
		&Handler{
			Path:         "/node/add",
			Callback:     editNode,
			CallbackArgs: []interface{}{D(""), "", R(":method"), R("title"), R("body")},
			OutputMapper: T{GetTemplate("node_edit"), "/node"},
		},
		&Handler{
			Path:         "/node/remove/",
			Callback:     removeNode,
			CallbackArgs: []interface{}{D(""), P(2), R(":method")},
			OutputMapper: T{GetTemplate("node_remove"), "/node"},
		},
	}
}

func getNodes(pers Persistence) []*Node {
	var rows []*Node
	if err := pers.LoadLimited("nodes", &rows, nil, 0, 100); err != nil {
		panic(err)
	}
	return rows
}

func editNode(pers Persistence, nid string, method, title, body string) (node *Node) {
	if method == "POST" {
		node = NewNode(title, body)
		if nid != "" {
			node.SetId(nid)
		}
		saveNode(pers, node)
	} else if nid != "" {
		node = getNode(pers, nid)
	} else {
		node = NewNode(title, body)
	}
	return
}

func removeNode(pers Persistence, nid string, method string) interface{} {
	if method == "POST" {
		if nid != "" {
			pers.Purge("nodes", IdSelector(nid))
		} else {
			panic(errors.New("empty nid given to remove"))
		}
	}
	// do nothing if request method is not POST
	return nil
}

func getNode(pers Persistence, nid string) *Node {
	var row *Node
	if err := pers.LoadOne("nodes", &row, IdSelector(nid)); err != nil {
		panic(err)
	}
	return row
}

func saveNode(pers Persistence, node *Node) {
	log.Printf("Saving node: %q\n", node)
	var err error
	if node.HasId() {
		err = pers.Save("nodes", node, node.Selector())
	} else {
		err = pers.Save("nodes", node, nil)
	}
	if err != nil {
		panic(err)
	}
}
